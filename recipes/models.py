from django.db import models

# Create your models here.


class Recipe(models.Model):
   name = models.CharField(max_length=125)
   author = models.CharField(max_length=100)
   description = models.TextField()
   image = models.URLField(null=True, blank=True)
   created = models.DateTimeField(auto_now_add=True)
   updated = models.DateTimeField(auto_now=True)

def __str__(self):
    return f"{self.title}"


class Step(models.Model):
    recipe = models.ForeignKey("Recipe", related_name="step", on_delete=models.CASCADE)
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation =models.CharField(max_length=10, unique=True)
    name = models.ForeignKey

    def __str__(self):
        return "{self.measure}"

class Fooditem(models.Model):
    name = models.CheckConstraint(max_length=100)

    def __str__(self):
        return "{self.Fooditem}"

class Ingredient(models.Model):
    fooditem = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE)

    def __str__(self):
        return "{self.Ingredient}"

class Tags(models.Model):
    name = models.CharField(max_length=20)
    recipes =models.ManyToManyField("Recipe", related_name="tags")

    def __str__(self):
        return "{self.Tags}"
