from django.config import admin
from recipes.models import Recipe, Step, Measure, Fooditem, Ingredient, Tags

# Register your models here.
admin.site.register(Recipe)
admin.site.register(Step)
admin.site.register(Measure)
admin.site.register(Fooditem)
admin.site.register(Ingredient)
admin.site.register(Tags)
